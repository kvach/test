FROM python:alpine3.7
COPY httptest.py /app/
WORKDIR /app
RUN  pip install urllib3
EXPOSE 8008
CMD python ./httptest.py
